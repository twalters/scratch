Scratch
=======

This repository is used to store small projects and prototypes that I build mostly for the learning experience or to experiment with some idea. 

Projects
--------

### Horsepower Converter

A small app to convert horsepower into something more interesting like Tyrannosaurus Rex power, space shuttle power, or tortoise power. 

### Node Image Rename

A tool to rename a directory of images based on old and new values in a JSON file.