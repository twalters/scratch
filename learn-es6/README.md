Learning EcmaScript 6
=====================

Notes and sample code for learning EcmaScript 6 through the [PluralSight JavaScript Fundamentals for ES6](http://www.pluralsight.com/courses/javascript-fundamentals-es6) course.